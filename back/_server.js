// Inicio definición cors

var express = require('express'),
  app = express(),
  cors = require('cors'),
  port = process.env.PORT || 3001

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(cors());

// Fin Definicion cors

//
// var express = require('express'),
//   app = express(),
//   port = process.env.PORT || 3001;
//
// //variable para poder usar el request-json
// var requestjson = require('request-json');
// //variable para que se pueda parsear el req.body y no salga como undefined
// var bodyParser = require('body-parser');
// app.use(bodyParser.json()); // for parsing application/json




var path = require('path');


// INI prueba POSTGRE
// Nombre de la máquina del docker. Máquina puerto y base de datos.
var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdproyectologin";
//var urlUsuarios = "postgres://docker:docker@localhost:5434/bdproyectologin";
var clientPostgre = new pg.Client(urlUsuarios); //lo podemos crear aquí o arriba
// FIN prueba POSTGRE

// INICIO Declaración variables mongodb
var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/local";
// FIN Declaración variables mongodb

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "apiKey=50c5ea68e4b0a97d668bc84a";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);

// INI prueba mi PROYECTO
//https://api.mlab.com/api/1/databases/proyectomovimientos/collections/movimientos?apiKey=dZm-4bp4cwi0AFmyuOpKziKXIuBlaUbl
var urlmovimientosMlabProyecto = "https://api.mlab.com/api/1/databases/proyectomovimientos/collections/movimientos";
var apiKeyProyecto = "apiKey=dZm-4bp4cwi0AFmyuOpKziKXIuBlaUbl";
var clienteMlab = requestjson.createClient(urlmovimientosMlabProyecto + "?" + apiKeyProyecto);
// FIN prueba mi PROYECTO

app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

// INICIO LOGIN POSTGRE
app.post('/login', function(req, res){
  //crear clientes postgreSQL

  console.log(req.body.email);
  clientPostgre.connect();
  const query = clientPostgre.query('SELECT * FROM usuarios WHERE email=$1 AND password=$2;', [req.body.email, req.body.password], (err, result) => {
  //const query = clientPostgre.query('SELECT * FROM usuarios WHERE email='req.body.email' AND 'req.body.password';', (err, result) => {
  //const query = clientPostgre.query('SELECT * FROM usuarios;', (err, result) => {

    if(err){
      console.log(err);
      res.send(err);
      console.log('salida erronea');
    }
    else{
      /*
      if(result.rows[0].count >= 1){
        res.send("Login correcto");
      }else{
        res.send("Login incorrecto");
      }
      */
      //console.log('es igual a undefined?? '+result.rows[0]=="undefined");
      //console.log('!result.rows.length?? '+(!result.rows.length));
      //console.log('NUMERO rows[0][0]?? '+result.rows[0]);
      if(!result.rows.length){
        console.log('USUARIO NO EXISTE EN BD');
        res.status(200).send(result.rows);
      }else{
        console.log('USUARIO EXISTE EN BD');
        res.status(200).send(result.rows);
      }
      //console.log('res.status(200).send(result.rows) '+res.status(200).send(result.rows));


    }
  })
  //Hacer consulta
  //Devolver resultado
});
// LOGIN FIN PRUEBA POSTGRE


// INICIO ALTA TRANSFERENCIAS/MOVIMIENTOS PROYECTO
app.post('/movimientosproyecto',function(req, res){



      console.log('urlmovimientosMlabProyecto --> '+urlmovimientosMlabProyecto);
      console.log('apiKeyProyecto --> '+apiKeyProyecto);

      clienteMlab = requestjson.createClient(urlmovimientosMlabProyecto + "?" + apiKeyProyecto);

      clienteMlab.post(urlmovimientosMlabProyecto + "?" + apiKeyProyecto, req.body, function(err, resultado, body){

        console.log('resultado.statusCode --> '+resultado.statusCode);
        if (resultado.statusCode == 200) {
          res.send (body);
        }
        else {
          res.sttusCode = res.statusCode;
          res.send ("");
        }
      });

});

// FIN ALTA TRANSFERENCIAS/MOVIMIENTOS PROYECTO

// INICIO PRUEBA ALTA USUARIOS ****POSTGRE**** OK
app.post('/altausuarios',function(req, res){

    console.log(req.body.email);
    clientPostgre.connect();
    const query = clientPostgre.query('INSERT INTO usuarios VALUES ($1, $2, $3, $4);', [req.body.email, req.body.password, req.body.dni, req.body.iban], (err, result) => {
    //const query = clientPostgre.query('SELECT * FROM usuarios WHERE email='req.body.email' AND 'req.body.password';', (err, result) => {
    //const query = clientPostgre.query('SELECT * FROM usuarios;', (err, result) => {

        console.log(req.body.email);
        console.log(req.body.password);
        console.log(req.body.dni);
        console.log(req.body.iban);
      if(err){
        console.log(err);
        res.send(err);
        console.log('salida erronea');
      }
      else{
        /*
        if(result.rows[0].count >= 1){
          res.send("Login correcto");
        }else{
          res.send("Login incorrecto");
        }
        */
        console.log('salida correcta del row? '+result.rows[0]);
        console.log('resulta.rows?? '+result.rows);
        res.status(200).send(result.rows);

      }
    })
});

// FIN PRUEBA ALTA USUARIOS POSTGRE




/*
PRUEBA Inicio con Mongo
*/
app.get('/movimientosmongo', function(req, res) {
  //alert('Estamos en el back');
  mongoClient.connect(url,function(err,db){
    if(err){

      console.log(body);

    }else{
      //alert(2);
      console.log("Connected successfully to server");

    var col = db.collection('movimientos');
    col.find({}).limit(3).toArray(function(err,docs){
    res.send(docs);
    });
      db.close();
    }
  })
});

app.post('/movimientosmongo',function(req, res){
  mongoClient.connect(url,function(err, db){
    var col = db.collection('movimientos');
    console.log("Traying to insert to server");
    /*
    db.collection('movimientos').insertOne({a:1}, function(err,r){
      console.log(r.insertedCount + ' registros insertados');
    });
    db.collection('movimientos').insertMany([{a:2},{a:3}], function(err,r){
      console.log(r.insertedCount + ' registros insertados body');
    });
    */
    db.collection('movimientos').insert(req.body, function(err,r){
       console.log(r.insertedCount + 'registros insertados body del PROYECTO');
    });

  db.close();
  res.send("ok");
  })
});
/*
PRUEBA FIN CON MONGO
*/


// Consulta movimientos PROYECTO
app.get('/movimientosproyecto', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  // apiKeyProyecto dZm-4bp4cwi0AFmyuOpKziKXIuBlaUbl
  console.log('urlmovimientosMlabProyecto 3333 --> '+urlmovimientosMlabProyecto);
  console.log('apiKeyProyecto --> '+apiKeyProyecto);
  clienteMlab = requestjson.createClient(urlmovimientosMlabProyecto + "?" + apiKeyProyecto);
  console.log('clienteMlab '+clienteMlab);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log('error '+err);
        console.log(body);
      }
      else
      {
        //console.log('Respuesta OK');
        console.log('Respuesta '+body);
        res.send(body);
      }
    });
});

// Prueba Fin PROYECTO


// // Consulta movimientos PROYECTO
// app.get('/movimientosproyecto', function(req, res) {
//   //res.sendFile(path.join(__dirname, 'index.html'));
//   // apiKeyProyecto dZm-4bp4cwi0AFmyuOpKziKXIuBlaUbl
//   console.log('urlmovimientosMlabProyecto --> '+urlmovimientosMlabProyecto);
//   console.log('apiKeyProyecto --> '+apiKeyProyecto);
//   clienteMlab = requestjson.createClient(urlmovimientosMlabProyecto + "?" + apiKeyProyecto);
//   console.log('clienteMlab '+clienteMlab);
//     clienteMlab.get('', function(err, resM, body) {
//       if(err)
//       {
//         console.log('error '+err);
//         console.log(body);
//       }
//       else
//       {
//         console.log('Respuesta OK + body --> ');
//         res.send(body);
//       }
//     });
// });

// Prueba Fin PROYECTO

app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?f={'idcliente':1, 'nombre': 1, 'apellidos': 1}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */

});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
})
